const mongoose = require('mongoose')

var schema = new mongoose.Schema({
    name: {
        type: String,
    },
    age: {
        type: Number,
    },
    genres: {
        type: String,
    },
    type: {
        type: String,
    },
})

var model = mongoose.model('Animales', schema)

module.exports = model