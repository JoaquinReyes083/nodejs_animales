var express = require('express');
var router = express.Router();
var Animales = require('../models/Animales')

/* GET users listing. */
router.post('/animales', (req, res) => {
    //req.body muestra los parametros que le mandas
    console.log(req.body)
    const animalNew = {
        name: req.body.name,
        age: req.body.age,
        genres: req.body.genres,
        type: req.body.type,
    }
    return new Animales(animalNew)
        Animales.find({})
        .exec()
        .then((doc) => {
            console.log('Animal grabado en la base de datos', doc)
            res.json(doc)
        })
        .catch((error) => {
            const message = 'Error al guardar animal en la DB'
            console.error(message, error)
            res.status(500).json({ message })
        })

    res.send('Creando al animal, aguarde un momento')
})
router.get('/animales', function (req, res) {
    Animales.find({})
        .exec()
        .then((docs) => {
            res.json(docs)
        })
        .catch((error) => {
            const message = 'Error en la DB al pedir datos'
            console.log(message, error)
            res.status(500).json({ message })
        })
});
router.get('/animales/:id', function (req, res) {
    const id = req.params.id
    Animales.findById({ id })
        .exec()
        .then((docs) => {
            res.json(docs)
        })
        .catch((error) => {
            const message = 'Error en la DB al pedir datos'
            console.log(message, error)
            res.status(500).json({ message })
        })
});
router.put('/animales/:id/name/:actu', function (req, res) {
    const id = req.params.id
    Animales.findByIdAndUpdate(id, { name: req.params.actu })
        .exec()
        .then((docs) => {
            res.send('Animal con el ID ' + id + ' ha sido actualizado')
            console.log('Animla con el ID ' + id + 'ha sido actualizado')
            res.json(docs)
        })
        .catch((error) => {
            const message = 'Error en la DB al actualizar datos'
            console.log(message, error)
            res.status(500).json({ message })
        })
});

router.delete('/animales/:id', function (req, res) {
    const id = req.params.id
    Animales.findByIdAndDelete({ id })
        .exec()
        .then((docs) => {
            res.json(docs)
        })
        .catch((error) => {
            const message = 'Error en la DB al borrar datos'
            console.log(message, error)
            res.status(500).json({ message })
        })
});
module.exports = router;
